import React, {Component} from 'react';
import './App.css';
import Game1 from './Game1';
import { Button,Container } from 'reactstrap';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      displayApp:true,//state of first page
      displayGame1: false,
      displayGame2:false,
      maxQuestionsNumber:5,
    }
    this.changeDisplayApp = this.changeDisplayApp.bind(this);
    this.chooseTheGame1 = this.chooseTheGame1.bind(this);
    this.chooseTheGame2 = this.chooseTheGame1.bind(this);
  }
  
  changeDisplayApp = ()=> {
   
    if (this.state.displayApp===true){
      this.setState({
        displayApp:false
      })
    } else{
      this.setState({
        displayApp:true,
        displayGame1: false,
       })
    }
  }

  chooseTheGame1 = ()=>{
    this.setState({displayApp:false})
      if (this.state.displayGame1===true){
            this.setState({
              displayGame1:false
              })
      } else{
            this.setState({
              displayGame1:true
          })
        }
    }

    chooseTheGame2 = ()=>{
      this.setState({displayApp:false})
        if (this.state.displayGame2===true){
              this.setState({
                displayGame2:false
                })
        } else{
              this.setState({
                displayGame2:true
            })
          }
      }

  render(){
    if (this.state.displayApp===true){
        return(
          <div className="container-fluid d-flex justify-content-center App">
            <div className=" col-9 justify-content-center">
              <div className="row justify-content-center  pb-5" >
                <h1 className= "display-3"> Which Game ?</h1>
              </div>
              <div className="row justify-content-center" >
                <div className=" col m-5">
                  <div className="row justify-content-center" >
                    <Button className="Button" 
                            size="lg"  
                            type="submit" 
                            onClick={()=>this.chooseTheGame1()}
                            >
                            game 1
                    </Button>
                  </div>
                  <div className="row justify-content-left Paragraph" >
                    <p>
                      (find the Capital of {this.state.maxQuestionsNumber} countries)
                    </p>
                  </div> 
                </div>
                <div className=" col m-5">
                  <div className = "row justify-content-center" >
                    <Button size="lg" 
                            className="Button" 
                            type="submit" 
                            onClick={()=>this.chooseTheGame2()}
                            >
                          game 2
                    </Button>
                  </div>
                  <div className="row justify-content-left Paragraph">
                    <p>find the maximum of capitals</p>
                  </div>
                </div>
             </div>
            </div>   
          </div>
        )
   }
    else{
      if(this.state.displayGame1 === true){
        return(
          <Container  className ="container-fluid d-flex justify-content-center App" >
            <Game1
                changeDisplayApp = {this.changeDisplayApp}
                chooseTheGame1 = {this.chooseTheGame1}
                maxQuestionsNumber={this.state.maxQuestionsNumber}
                />
          </Container>
        )
        }
      else if(this.state.displayGame2 === true){
            return(
               <Container  className ="container-fluid d-flex justify-content-center App" >
                <Game1
                  changeDisplayApp = {this.changeDisplayApp}
                  chooseTheGame2 = {this.chooseTheGame2}
                  maxQuestionsNumber={this.state.maxQuestionsNumber}
                  />
                </Container>
                )
            }
    }
  }
}

export default App;
