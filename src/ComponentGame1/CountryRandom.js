import React,{ Component} from 'react';
import EndScoreDisplay from './EndScoreDisplay';
import DisplayRightResponse from './DisplayRightResponse';
import { Button} from 'reactstrap';

class CountryRandom extends Component{

    constructor(props){
        super(props);
        this.state={
            stateMainPage:true,
            rightAnswer:0,
            displayAnswer:false,
            rightAnswerCount:0,
            questionsNumber:0,
            stateDisplayRightResponse:true,
            cSelected: [],
        }
        this.handleChangeStateMainPage = this.handleChangeStateMainPage.bind(this);
        this.resetQuestionsNumbers=this.resetQuestionsNumbers.bind(this);
        this.handleChangestateDisplayRightResponseTrue=this.handleChangestateDisplayRightResponseTrue.bind(this);
        this.handleChange=this.handleChange.bind(this);
        this.onCheckboxBtnClick = this.onCheckboxBtnClick.bind(this);
    }
    
    makeAnswersTab = (capitalsNameTab) => {
        let answersTab = [ capitalsNameTab[Math.floor(Math.random() * capitalsNameTab.length)],
                           capitalsNameTab[Math.floor(Math.random() * capitalsNameTab.length)],
                           capitalsNameTab[Math.floor(Math.random() * capitalsNameTab.length)]
                      ];
          for (let i=0; i<=3; i++){
            while(answersTab[i]===""){
                answersTab[i]=capitalsNameTab[Math.floor(Math.random() * capitalsNameTab.length)]
            }
          }
          if (this.props.chosenCountry=== undefined |this.props.chosenCountry===null ){return ''};
          answersTab.push(this.props.chosenCountry.capital);
          // mix the arrayanswersTab
          let currentIndex = answersTab.length;
          let temporaryValue;
          let randomIndex = currentIndex;
          while (0 !==currentIndex){
              randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
          temporaryValue = answersTab[currentIndex];
          answersTab[currentIndex] = answersTab[randomIndex];
          answersTab[randomIndex] = temporaryValue; 
          };
          return (answersTab)
    }

     onCheckboxBtnClick(selected) {
        this.setState({displayAnswer:true});
            if (selected===
                this.props.chosenCountry.capital){
                        this.setState({ rightAnswer:1,
                                        rightAnswerCount:this.state.rightAnswerCount+1})
                                        }
            else{this.setState({rightAnswer:2})}
        this.setState(
                {questionsNumber:this.state.questionsNumber+1}
            )                            
        }


    handleChange= ()=> {
        this.setState({ displayAnswer:false,
                         });
        this.props.anotherChosenCountry(this.props.countries)
            }
    
    handleEnd = ()=>{
        this.setState(
            {questionsNumber:this.state.questionsNumber+1,
                            
                        }
                    )                    
        }
        
        handleChangestateDisplayRightResponse = ()=>{
            this.setState({questionsNumber:this.state.questionsNumber+1,
                stateDisplayRightResponse:false
            })
            this.handleChangeStateMainPage(); 
            }
        handleChangestateDisplayRightResponseTrue = ()=>{
            this.setState({
                stateDisplayRightResponse:true
            })
             
            }    

    handleChangeStateMainPage = ()=>{
        let stateMainPage = this.state.stateMainPage
        if(stateMainPage=== true){
            this.setState({ stateMainPage: false})
        }else { 
            this.setState({ stateMainPage: true})    
        }
    }

    displayCountry= (country)=>{
        if (country===[] | country===undefined |country===null){
                return null;
                    }
        let name = country.name;
        return name;
        }

    resetQuestionsNumbers=(questionsNumber,rightAnswerCount) =>{
        this.setState({questionsNumber:0,
                      rightAnswerCount:0})
    }
    
    render(){
        const solutions = this.makeAnswersTab(this.props.allCapitalsNameTab);
        let compt=this.state.questionsNumber;  
        let displayRightResponseLink=
                <DisplayRightResponse   rightAnswer = {this.state.rightAnswer} 
                                        chosenCountry= {this.props.chosenCountry}
                                        rightAnswerCount= {this.state.rightAnswerCount} 
                                        questionsNumber= {this.state.questionsNumber}
                                        handleChangestateDisplayRightResponse = {this.state.handleChangestateDisplayRightResponse}
                                        stateDisplayRightResponse = {this.state.stateDisplayRightResponse}
                                        anotherChosenCountry={this.props.anotherChosenCountry}
                                        handleChangeStateMainPage = {this.handleChangeStateMainPage}                       
                />
        if (this.state.stateMainPage === true){
            if(compt<this.props.maxQuestionsNumber){
                if (this.state.displayAnswer===false){
                return(
                <div className="col-9 justify-content-center">    
                    <div className="row justify-content-center  pb-1"> 
                        <h1 className="display-4">What's the capital of  </h1>
                    </div>
                    <div className="row justify-content-center  pb-1"> 
                        <h1 className="display-3">
                            {this.displayCountry(this.props.chosenCountry)} ?
                        </h1> 
                    </div>
                    <div className="row justify-content-center Paragraph pb-5"> 
                        <p>
                            is it?
                        </p>
                    </div>  
                    
                    <form name = "answers">
                        <div className = "row justify-content-center">
                            <div className="col justify-content-center">
                                <div className = "row justify-content-center pr-2">
                                    <Button className="Button"
                                            onClick={() => this.onCheckboxBtnClick(solutions[0])} 
                                            >
                                        {solutions[0]}
                                    </Button>  
                                </div>
                                <div className = "row justify-content-center pr-2">
                                    <Button onClick={() => this.onCheckboxBtnClick(solutions[1])} 
                                            className="Button"
                                            >
                                        {solutions[1]}
                                    </Button> 
                                </div>
                            </div>
                            <div className="col justify-content-center">
                                <div  className = "row justify-content-center pl-2">
                                <Button className="Button"     
                                        onClick={() => this.onCheckboxBtnClick(solutions[2])} 
                                        >
                                    {solutions[2]}
                                </Button>
                                </div>
                                <div  className = "row justify-content-center pl-2">
                                <Button className="Button"     
                                        onClick={() => this.onCheckboxBtnClick(solutions[3])} 
                                        >
                                        {solutions[3]}
                                </Button>
                                </div>
                            </div>
                        </div> 
                    </form>
                    {/* <div className = "row justify-content-center">
                        <button type="submit"
                                className="Button" 
                                onClick={()=>this.handleSubmit()}
                                >
                            Submit
                        </button>
                    </div> */}
                </div>
                )
                    }else
                        {
                        return(
                                <div className="col  AnswerPage justify-content-center">
                                    <div className="row justify-content-center">
                                        {displayRightResponseLink}
                                    </div>
                                    <div className="row justify-content-center"> 
                                        <button type="submit" 
                                                onClick={()=>this.handleChange()}
                                                className="Button"
                                                >
                                            new Question
                                        </button>
                                    </div>                      
                                </div>
                            )
                        }
                    }else if (compt===this.props.maxQuestionsNumber) 
                                {
                                return(
                                        <div className="col  AnswerPage justify-content-center">
                                            <div className="row justify-content-center">
                                                {displayRightResponseLink}  
                                            </div>
                                            <div className="row justify-content-center">
                                                <button type="submit"   
                                                        onClick={()=>this.handleChangestateDisplayRightResponse()}
                                                        className="Button"
                                                    >
                                                    your score
                                                </button>
                                            </div>
                                        </div>
                                    )
                    } else {
                            return(
                                    <div className="col  AnswerPage justify-content-center">
                                        {displayRightResponseLink}  
                                    </div>
                                )
                        }
        } else {
                return(
                        <div className="col  AnswerPage justify-content-center">
                            <EndScoreDisplay 
                                rightAnswerCount= {this.state.rightAnswerCount} 
                                questionsNumber= {this.state.questionsNumber}
                                anotherChosenCountry={this.props.anotherChosenCountry}
                                handleChangeStateMainPage = {this.handleChangeStateMainPage}
                                resetQuestionsNumbers ={this.resetQuestionsNumbers}
                                handleChangestateDisplayRightResponseTrue={this.handleChangestateDisplayRightResponseTrue}
                                handleChange={this.handleChange}
                                changeDisplayApp = {this.props.changeDisplayApp}
                                chooseTheGame1 = {this.props.chooseTheGame1}
                            />
                        </div>
                    )
        }
        
        

    }
}

export default CountryRandom;