import React, {Component} from 'react';

class EndScoreDisplay extends Component {

    state= {
        display:true
    }

    handleTryAgain= ()=> {
        this.setState({ display:false,
                         });
        this.props.anotherChosenCountry(this.props.countries);
        this.props.handleChangeStateMainPage();
        this.props.resetQuestionsNumbers(this.props.questionsNumber,this.props.rightAnswerCount);
        this.props.handleChangestateDisplayRightResponseTrue();
        this.props.handleChange();
            }
    handleNewGame = ()=> {
        this.props.changeDisplayApp();
    }        

    render(){

        if(this.state.display ===true){
            let ajustement = this.props.questionsNumber -1;
            return(
                    <div className ="row Paragraph">
                        <div className = "col">
                            <h1>
                                Your Score is:
                            </h1> 
                            <h1>
                                {this.props.rightAnswerCount}  / {ajustement}
                            </h1>
                            <p>
                            <button type="submit" 
                                    onClick={()=>this.handleTryAgain()}
                                    className = "ButtonEnd"
                                    >Try Again
                            </button>
                            </p>
                            <p>
                                <button type="submit" 
                                        onClick={()=>this.handleNewGame()}
                                        className = "ButtonEnd"
                                        >
                                    Another Game
                                </button>
                            </p>
                        </div>
                    </div>
                    )
        }
    }
}

export default EndScoreDisplay;