import React, {Component} from 'react';
import CountryRandom from './ComponentGame1/CountryRandom';



class Game1 extends Component {

  constructor() {
    super();
    this.state = {
    countries:[],
    chosenCountry:[],
    allCapitalsNameTab: [],
    isLoaded:false,
    }
    this.anotherChosenCountry = this.anotherChosenCountry.bind(this);
  }
  refsub = React.createRef();
  // fetch API, after in countries you have got a tab of countries 's objects   
  componentDidMount() {
    fetch('https://restcountries.com/v2/all')
      .then(res => res.json())
      .then((data) => {
        this.setState({
          countries: data,
          chosenCountry:this.randomCountry(data),
          allCapitalsNameTab:this.randomCapital(data),
          isLoaded:true,
        })
      })
      .catch(console.log)
  }
  randomCountry= (countries)=>{
    if(countries === undefined|countries === null) {
        return ["essai",'name'];
    }
    let index=Math.floor(Math.random() * countries.length);
    let chosCountry = countries[index];
    if (chosCountry.capital===""){
      chosCountry.capital="pas de capital"
    }
    return chosCountry
  }

//make the tab of answers (the right one + 3 random others)
  randomCapital = (countries) => {
      let allCapitalsNameTab = [];
      if(countries === undefined |countries===[]) {
          return ["name",'essai'];
        }
      for (let i = 0; i < countries.length; i++) {
        let country = countries[i];
        allCapitalsNameTab.push(country.capital);
      };
     
     return (allCapitalsNameTab)
    }

    anotherChosenCountry = ()=>{
      let newChosenCountry =this.randomCountry(this.state.countries);
      if (newChosenCountry.capital===""){
        newChosenCountry.capital="pas de capital"
      }
      this.setState(
        {
          chosenCountry: newChosenCountry,
         });
    }


  render() {
  if (this.state.isLoaded=== false){ return 'wait'}
  else{
    let chosCountry= this.randomCountry(this.state.countries);
    return ( 
      <div className="col d-flex justify-content-center" >
          <CountryRandom  countries = {this.state.countries} 
                          chosenCountry ={chosCountry} 
                          answersTab={this.randomCapital(chosCountry)} 
                          allCapitalsNameTab={this.state.allCapitalsNameTab}
                          anotherChosenCountry={this.anotherChosenCountry}
                          changeDisplayApp = {this.props.changeDisplayApp}
                          chooseTheGame1 = {this.props.chooseTheGame1}
                          maxQuestionsNumber={this.props.maxQuestionsNumber}
          />
      </div>
    )
  }}
}

export default Game1;